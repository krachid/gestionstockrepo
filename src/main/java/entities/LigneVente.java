package entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LigneVente implements Serializable{
	
	
	@Id
	@GeneratedValue
	private long idLigneVente;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	
	@ManyToOne
	@JoinColumn(name="idVente")
	private Article vente;

	public long getIdFournisseur() {
		return idLigneVente;
	}

	public void setIdFournisseur(long idLigneVente) {
		this.idLigneVente = idLigneVente;
	}

	public long getIdLigneVente() {
		return idLigneVente;
	}

	public void setIdLigneVente(long idLigneVente) {
		this.idLigneVente = idLigneVente;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Article getVente() {
		return vente;
	}

	public void setVente(Article vente) {
		this.vente = vente;
	}
	
}
