package entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LigneCommandeClient implements Serializable{
	
	
	@Id
	@GeneratedValue
	private long idLigneCommandeClient;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	
	@ManyToOne
	@JoinColumn(name="idCmdClient")
	private CmdClient commandeClient;

	public long getIdFournisseur() {
		return idLigneCommandeClient;
	}

	public void setIdFournisseur(long idLigneCommandeClient) {
		this.idLigneCommandeClient = idLigneCommandeClient;
	}

	public long getIdLigneCommandeClient() {
		return idLigneCommandeClient;
	}

	public void setIdLigneCommandeClient(long idLigneCommandeClient) {
		this.idLigneCommandeClient = idLigneCommandeClient;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CmdClient getCommandeClient() {
		return commandeClient;
	}

	public void setCommandeClient(CmdClient commandeClient) {
		this.commandeClient = commandeClient;
	}
	
	
}
