package entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class MvtStock implements Serializable {
	
	public static final int ENTREE = 1;
	public static final int SORTIE = 2;
	
	
	@Id
	@GeneratedValue
	private long idMvtStock;
	
	@Temporal(TemporalType.TIME)
	private Date dateMvtStk;
	
	private BigDecimal quantite;
	
	private int typeMvt;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;

	public long getIdvente() {
		return idMvtStock;
	}

	public void setIdvente(long idMvtStock) {
		this.idMvtStock = idMvtStock;
	}

	public long getIdMvtStock() {
		return idMvtStock;
	}

	public void setIdMvtStock(long idMvtStock) {
		this.idMvtStock = idMvtStock;
	}

	public Date getDateMvtStk() {
		return dateMvtStk;
	}

	public void setDateMvtStk(Date dateMvtStk) {
		this.dateMvtStk = dateMvtStk;
	}

	public BigDecimal getQuantite() {
		return quantite;
	}

	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}

	public int getTypeMvt() {
		return typeMvt;
	}

	public void setTypeMvt(int typeMvt) {
		this.typeMvt = typeMvt;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
	
}
